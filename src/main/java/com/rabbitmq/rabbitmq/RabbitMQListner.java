package com.rabbitmq.rabbitmq;

import com.rabbitmq.rabbitmq.db.repository.UserTaskRepository;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQListner implements MessageListener {

    @Autowired
    UserTaskRepository userTaskRepository;

    public void onMessage(Message message) {
        // bakalım ...
        System.out.println("Consuming Message - " + new String(message.getBody()));
    }

}