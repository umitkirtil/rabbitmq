package com.rabbitmq.rabbitmq;


import java.util.ArrayList;
import java.util.List;

public class UserTaskDTO {

    //from users table ...
    private String userid;

    //from Tasks table ...
    private List<String> tasks = new ArrayList<>();

    public UserTaskDTO() {
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "UserTaskDTO{" +
                "userid='" + userid + '\'' +
                ", tasks sayısı=" + tasks.size() +
                '}';
    }
}
