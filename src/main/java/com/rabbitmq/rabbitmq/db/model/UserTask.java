package com.rabbitmq.rabbitmq.db.model;

import javax.persistence.*;

@Entity
public class UserTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "userid", nullable = false)
    private Long userID;

    @Column(name = "taskname", nullable = false)
    private String taskName;

    @Column(name = "taskid", nullable = false)
    private Long taskID;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
