package com.rabbitmq.rabbitmq.db.repository;

import com.rabbitmq.rabbitmq.db.model.UserTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserTaskRepository extends JpaRepository<UserTask, Long> {

    Optional<UserTask> findUserTaskByUserID(Long userID);
}
